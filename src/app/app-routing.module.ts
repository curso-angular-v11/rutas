import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InfoComponent } from './componentes/info/info.component';
import { ContactComponent } from './componentes/contact/contact.component';
import { AboutComponent } from './componentes/about/about.component';
import { DetalleEmpleadoComponent } from './componentes/detalle-empleado/detalle-empleado.component';
import { ProyectosComponent } from './componentes/empleados/proyectos/proyectos.component';
import { ExperienciaComponent } from './componentes/empleados/experiencia/experiencia.component';
import { CurriculumComponent } from './componentes/empleados/curriculum/curriculum.component';
import { RandomGuard } from './random.guard';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'informacion'}, //ruta raíz
  {path: 'informacion', component: InfoComponent, canActivate: [RandomGuard]},
  {path: 'sobre', component: AboutComponent},
  {path: 'contacto', component: ContactComponent},
  {path: 'about', redirectTo: 'sobre'},
  {
    path: 'empleados/:empleadoId', component: DetalleEmpleadoComponent, children:[
      {path: 'proyectos', component: ProyectosComponent},
      {path: 'experiencia', component: ExperienciaComponent},
      {path: 'curriculum', component: CurriculumComponent}
    ]
  },
  {path: '**', redirectTo: 'contacto'}                      //ruta comodin
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
